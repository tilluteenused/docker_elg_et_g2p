#!/usr/bin/env python3

import re
import sys
import json
import subprocess
from elg import FlaskService
from elg.model import TextsResponse, TextRequest

'''
# command line script
./create_venv.sh
python3 -m venv venv_elg_estnltk
venv_elg_estnltk/bin/python3 ./elg_sdk_preproc.py --json='{"type":"text","content":"park OECD ABC-pood"}'

# web server in docker & curl
curl -i --request POST --header "Content-Type: application/json" --data '{"type":"text","content":"park OECD ABC-pood"}' 127.0.0.1:5000/process
'''

class et_g2p(FlaskService):
    def process_text(self, request: TextRequest) -> TextsResponse:
        '''
        Find sentences and tokens
        :param content: {TextRequest} - input text in ELG TextRequest format
        :return: {TextsResponse} -  output in ELG-format
        '''
        txt = '\n'.join(request.content.split())
        path = 'github_et_g2p'
        proc = subprocess.Popen([f'{path}/run.sh'], text=True,
            stdout=subprocess.PIPE, stdin=subprocess.PIPE,
                                    stderr=subprocess.DEVNULL)
        proc.stdin.write(txt)
        proc.stdin.close()
        lines = proc.stdout.read().splitlines()
        texts_out = []
        for line in lines:
            c_and_f = line.split('\t')
            assert len(c_and_f) == 2
            if (len(texts_out) > 0) and (re.match(r"^"+texts_out[-1]["content"]+"\(\d+\)$", c_and_f[0]) is not None):
                texts_out[-1]["features"]["analysis"].append(c_and_f[1])
            else:    
                texts_out.append({"content": c_and_f[0], "features": {"analysis":[c_and_f[1]]}})
        return TextsResponse(texts=texts_out)

flask_service = et_g2p("Grapheme to phoneme for Estonian")
app = flask_service.app


def run_test(my_query_str: str) -> None:
    '''
    Run as command line script
    :param my_query_str: input in json string
    '''
    my_query = json.loads(my_query_str)
    service = et_g2p("TartuNLP tts preproc et")
    request = TextRequest(content=my_query["content"])
    response = service.process_text(request)

    response_json_str = response.json(exclude_unset=True)  # exclude_none=True
    response_json_json = json.loads(response_json_str)
    return response_json_json

def run_server() -> None:
    '''
    Run as flask webserver
    '''
    app.run()

if __name__ == '__main__':
    import argparse
    argparser = argparse.ArgumentParser(allow_abbrev=False)
    argparser.add_argument('-j', '--json', type=str, help='sentences to precrocess')
    args = argparser.parse_args()
    if args.json is None:
        run_server()
    else:
        json.dump(run_test(args.json), sys.stdout, indent=4)
